import json
import sys
from login import conn
from login import headers
from monitorsSummary import getMonitorsSummary
from activate import putActivate
from probe import putProbe
 
if len(sys.argv) != 2:
    raise ValueError('Flap region from active to inactive or vice versa')

jsonMonitors = json.loads(getMonitorsSummary(headers,conn))

#print(jsonMonitors)

for probe in jsonMonitors:
    if (sys.argv[1] in probe['name']):
        probe['active'] = not probe['active']
        putProbe(headers, conn, probe)

putActivate(headers, conn)
