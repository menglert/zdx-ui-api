import http.client
import json
import getopt
import sys
import getpass
import keyring

try:
    configFile = open("config.json")
    configJson = json.load(configFile)
    adminUser = configJson["adminUser"]
    zdxCloud = configJson["zdxCloud"]
    adminPassword = keyring.get_password("zdx-api-user", adminUser)
except IOError:
    print("No config present. Creating one.")
    configJson = {}
    adminUser = input("Admin User: ")
    adminPassword = getpass.getpass(prompt="Admin Password: ")
    zdxCloud = input("Zscaler Cloud: ")
    configJson["adminUser"] = adminUser
    configJson["zdxCloud"] = zdxCloud
    configFile = open("config.json", "w")
    json.dump(configJson, configFile)
    configFile.close()
    keyring.set_password("zdx-api-user", adminUser, adminPassword)
finally:
    configFile.close()

conn = http.client.HTTPSConnection(zdxCloud)

payload = {"username": adminUser, "password": adminPassword}

headers = {
    'content-type': "*/*",
    'cache-control': "no-cache",
    'X-CSRF-Token': 'Fetch'
}

conn.request("GET", "/zdx/api/v1/auth", headers=headers)

res = conn.getresponse()
data = res.read()
setCookie = res.headers.get_all("Set-Cookie")
csrfToken = res.headers.get("X-CSRF-Token")
jSessionId = setCookie[0].split(";")[0]

headers = {
    'content-type': "application/json",
    'cookie': jSessionId,
    'Sec-Fetch-Site': "same-origin",
    'Sec-Fetch-Mode': "cors",
    'Sec-Fetch-Dest': "empty",
    'X-CSRF-Token': csrfToken
}

conn.request("POST", "/zdx/api/v1/auth", body=json.dumps(payload), headers=headers)

res = conn.getresponse()
data = res.read()

#print(data.decode("utf-8"))
#print(res.headers)
#print(headers)