def getMonitorsSummary(headers, conn):
    conn.request("GET", "/zdx/api/v1/monitors/summary?pageSize=512", headers=headers)

    res = conn.getresponse()
    data = res.read()

    #print(data.decode("utf-8"))
    return data