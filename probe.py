import json

def putProbe(headers, conn, probe):
    conn.request("PUT", "/zdx/api/v1/applications/" + str(probe['application']['id']) + "/monitors/" +  str(probe['id']), body=json.dumps(probe), headers=headers)

    res = conn.getresponse()
    data = res.read()

    #print(data.decode("utf-8"))
    return data